//
//  WalletManager.swift
//  BreadWalletIntegration
//
//  Created by Evgeniy Staroselskiy on 2/13/18.
//  Copyright © 2018 Yevhen Staroselskyi. All rights reserved.
//

import Foundation
import UIKit
import BRCore

struct KeychainKey {
    public static let mnemonic = "mnemonic"
    public static let creationTime = "creationtime"
    public static let masterPubKey = "masterpubkey"
    public static let spendLimit = "spendlimit"
    public static let pin = "pin"
    public static let pinFailCount = "pinfailcount"
    public static let pinFailTime = "pinfailheight"
    public static let apiAuthKey = "authprivkey"
    public static let userAccount = "https://api.breadwallet.com"
    public static let seed = "seed" // deprecated
    public static let keyArchiver = "keyarchiver"
}


struct KeysArchiver {
    var publicKey: String
    var privateKey: String
    
    init(publicKey: String, privateKey: String) {
        self.publicKey = publicKey
        self.privateKey = privateKey
    }
}

class WalletManager {
    
    private let WalletSecAttrService = "com.breadwallet.integration"
    private let BIP39CreationTime = TimeInterval(BIP39_CREATION_TIME) - NSTimeIntervalSince1970
    internal var didInitWallet = false
    var masterPubKey = BRMasterPubKey()
    
    var wallet: BRWallet? {
        guard self.masterPubKey != BRMasterPubKey() else { return nil }
        guard let wallet = lazyWallet else {
            // stored transactions don't match masterPubKey
            //            #if !Debug
            //                do { try FileManager.default.removeItem(atPath: self.dbPath) } catch { }
            //            #endif
            return nil
        }
        self.didInitWallet = true
        return wallet
    }
    
    internal lazy var lazyWallet: BRWallet? = {
        return BRWallet(transactions: [BRTxRef?](), masterPubKey: self.masterPubKey,
                        listener: nil)
    }()
    
    init(store: String, dbPath: String? = nil) throws {
        if !UIApplication.shared.isProtectedDataAvailable {
            throw NSError(domain: NSOSStatusErrorDomain, code: Int(errSecNotAvailable))
        }
        
        let mpkData: Data? = try keychainItem(key: KeychainKey.masterPubKey)
        guard let masterPubKey = mpkData?.masterPubKey else {
            //try self.init(masterPubKey: BRMasterPubKey(), earliestKeyTime: 0, dbPath: dbPath, store: store)
            return
        }
        
        var earliestKeyTime = BIP39CreationTime
        if let creationTime: Data = try keychainItem(key: KeychainKey.creationTime),
            creationTime.count == MemoryLayout<TimeInterval>.stride {
            creationTime.withUnsafeBytes { earliestKeyTime = $0.pointee }
        }
        
        //try self.init(masterPubKey: masterPubKey, earliestKeyTime: earliestKeyTime, dbPath: dbPath, store: store)
    }
    
    func setupKeys() {
        do {
            var asdas = BRKey()

            let phrase = "1234"
            guard let nfkdPhrase = CFStringCreateMutableCopy(secureAllocator, 0, phrase as CFString)
                else { return }
            CFStringNormalize(nfkdPhrase, .KD)
            var seed = UInt512()
            try setKeychainItem(key: KeychainKey.mnemonic, item: nfkdPhrase as String?, authenticated: true)
            BRBIP39DeriveKey(&seed, nfkdPhrase as String, nil)
            self.masterPubKey = BRBIP32MasterPubKey(&seed, MemoryLayout<UInt512>.size)
            seed = UInt512() // clear seed
            try setKeychainItem(key: KeychainKey.masterPubKey, item: Data(masterPubKey: self.masterPubKey))
            

            
            var key = BRKey()
            BRBIP39DeriveKey(&seed, phrase, nil)
            BRBIP32APIAuthKey(&key, &seed, MemoryLayout<UInt512>.size)  // EE PRIVATE KEY
            seed = UInt512() // clear seed
            let pkLen = BRKeyPrivKey(&key, nil, 0)
            var pkData = CFDataCreateMutable(secureAllocator, pkLen) as Data
            pkData.count = pkLen
            guard pkData.withUnsafeMutableBytes({ BRKeyPrivKey(&key, $0, pkLen) }) == pkLen else { return  }
            let privKey = CFStringCreateFromExternalRepresentation(secureAllocator, pkData as CFData,
                                                                   CFStringBuiltInEncodings.UTF8.rawValue) as String
            try setKeychainItem(key: KeychainKey.apiAuthKey, item: privKey)
        }
        catch {
            return
        }
    }
    
    func keychainItem<T>(key: String) throws -> T? {
        let query = [kSecClass as String : kSecClassGenericPassword as String,
                     kSecAttrService as String : WalletSecAttrService,
                     kSecAttrAccount as String : key,
                     kSecReturnData as String : true as Any]
        var result: CFTypeRef? = nil
        let status = SecItemCopyMatching(query as CFDictionary, &result);
        guard status == noErr || status == errSecItemNotFound else {
            throw NSError(domain: NSOSStatusErrorDomain, code: Int(status))
        }
        guard let data = result as? Data else { return nil }
        
        switch T.self {
        case is Data.Type:
            return data as? T
        case is String.Type:
            return CFStringCreateFromExternalRepresentation(secureAllocator, data as CFData,
                                                            CFStringBuiltInEncodings.UTF8.rawValue) as? T
        case is Int64.Type:
            guard data.count == MemoryLayout<T>.stride else { return nil }
            return data.withUnsafeBytes { $0.pointee }
        case is Dictionary<AnyHashable, Any>.Type:
            return NSKeyedUnarchiver.unarchiveObject(with: data) as? T
        default:
            throw NSError(domain: NSOSStatusErrorDomain, code: Int(errSecParam))
        }
    }
    
    func setKeychainItem<T>(key: String, item: T?, authenticated: Bool = false) throws {
        let accessible = (authenticated) ? kSecAttrAccessibleWhenUnlockedThisDeviceOnly as String
            : kSecAttrAccessibleAfterFirstUnlockThisDeviceOnly as String
        let query = [kSecClass as String : kSecClassGenericPassword as String,
                     kSecAttrService as String : WalletSecAttrService,
                     kSecAttrAccount as String : key]
        var status = noErr
        var data: Data? = nil
        if let item = item {
            switch T.self {
            case is Data.Type:
                data = item as? Data
            case is String.Type:
                data = CFStringCreateExternalRepresentation(secureAllocator, item as! CFString,
                                                            CFStringBuiltInEncodings.UTF8.rawValue, 0) as Data
            case is Int64.Type:
                data = CFDataCreateMutable(secureAllocator, MemoryLayout<T>.stride) as Data
                [item].withUnsafeBufferPointer { data?.append($0) }
            case is Dictionary<AnyHashable, Any>.Type:
                data = NSKeyedArchiver.archivedData(withRootObject: item)
            default:
                throw NSError(domain: NSOSStatusErrorDomain, code: Int(errSecParam))
            }
        }
        
        if data == nil { // delete item
            if SecItemCopyMatching(query as CFDictionary, nil) != errSecItemNotFound {
                status = SecItemDelete(query as CFDictionary)
            }
        }
        else if SecItemCopyMatching(query as CFDictionary, nil) != errSecItemNotFound { // update existing item
            let update = [kSecAttrAccessible as String : accessible,
                          kSecValueData as String : data as Any]
            status = SecItemUpdate(query as CFDictionary, update as CFDictionary)
        }
        else { // add new item
            let item = [kSecClass as String : kSecClassGenericPassword as String,
                        kSecAttrService as String : WalletSecAttrService,
                        kSecAttrAccount as String : key,
                        kSecAttrAccessible as String : accessible,
                        kSecValueData as String : data as Any]
            status = SecItemAdd(item as CFDictionary, nil)
        }
        
        guard status == noErr else {
            throw NSError(domain: NSOSStatusErrorDomain, code: Int(status))
        }
    }
    
}
