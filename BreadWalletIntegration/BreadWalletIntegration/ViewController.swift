//
//  ViewController.swift
//  BreadWalletIntegration
//
//  Created by Evgeniy Staroselskiy on 2/9/18.
//  Copyright © 2018 Yevhen Staroselskyi. All rights reserved.
//

import UIKit
import BRCore



class ViewController: UIViewController {
    
    @IBOutlet weak var showKeysButton: UIButton!
    @IBOutlet weak var createWalletButton: UIButton!
    @IBOutlet weak var privateKeyLabel: UILabel!
    @IBOutlet weak var publicKeyLabel: UILabel!
    private var walletManager: WalletManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
        crateManager()
    }
    
    func configure()  {
        
    }
    
    
    @IBAction func createWalletButtonAction(_ sender: Any) {
        self.walletManager!.setupKeys()
    }
    
    @IBAction func showKeysButtonAction(_ sender: Any) {
        do {
            let mpkData: Data? = try walletManager?.keychainItem(key: KeychainKey.masterPubKey)
            let privateKey: String? = try walletManager?.keychainItem(key: KeychainKey.apiAuthKey)
            self.publicKeyLabel.text = "\(String(describing: mpkData?.masterPubKey?.pubKey))"
            self.privateKeyLabel.text = privateKey
        }
        catch {
            return
        }
    }
    
    func crateManager() {
        DispatchQueue.main.async {
            do {
                self.walletManager = try WalletManager(store: "", dbPath: nil)
                let _ = self.walletManager?.wallet
            } catch let error {
                assert(false, "Error creating new wallet: \(error)")
            }
        }
    }
}

